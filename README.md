# 主题网页

一、设计主题：兰大猫协

二、页面结构：主页、蹲领养页、公告通知页、以及配套的手机首页
配套手机首页尺寸如下：390×844，参考iPhone 12 Pro 

三、动画说明
注：点开时100%的页面展示可能太大了，在Microsoft edge中75%更合适观看。
主页实现的效果：
1.二级菜单
本网站导航栏中的 “精彩合作” 小板块具有二级菜单。

2.可链接的网页
导航栏中的”精彩合作”栏目下的二级菜单的”猫咪领养”可转到”蹲领养.html”；
导航栏中的”信息公告”栏目可转到”公告页.html”。

3.最顶端主页图片轮播，并且鼠标悬浮在上时停止轮播，鼠标移开继续轮播
javascript实现，轮播时2s自动换图片，共5张图片，且轮播到对应图片时其小标变色（变黄）

4.最顶端主页图片配上对应小标，可以通过对应小标手动变换展示图片
javascript实现

5.最顶端主页图片配有左右箭头，可手动切换到前一张或后一张展示图片，同时对应小标变色
javascript实现

6.中间页面图片走马灯展示，同样鼠标悬浮在上时停止轮播，鼠标移开继续轮播
原网页为视频播放，为尽量还原，放了图片的滑动走马灯展示

7.在蓝色大块的“救助猫咪 美好生活”板块，其下大标题和logo可在鼠标悬浮时逐渐变大
css实现，transition实现过渡自然，transform:scale(1.2)实现放大

8.在"news"板块，每张图片当鼠标悬浮在上时图片消失，显示说明文字
javascript实现

9.在”联系我们”栏目，有两个不断弹跳的svg箭头图案
Javascript动画库实现
还有一个鼠标悬浮时会心跳状变色的按钮”发送”
css库实现

蹲领养页实现的效果：
1.最顶端主页图片模块，图片轮播，小标设置为圆形，小标背景为对应图片主题的猫咪
在播放到相应图片时，圆形小标实现放大和边框变黄色
同样设有左右箭头用以前后切换图片
javascript+css实现

2.猫咪展示框，在鼠标移到对应展示框上方时，展示框左移，由原来的展示50%到完全展示。
javascript实现

公告页实现的效果：
1.”通知公告”字体实现不断左右平移
javascript动画库实现

2.最后的栏目有两个鼠标悬浮时变色同时上下跳动的按钮框
Css+javascript动画库实现

